<?php

namespace App;

abstract class Payment
{

    abstract  public function makePayment($total, $amount);

}