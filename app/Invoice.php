<?php

namespace App;

class Invoice
{

    public $reference;
    public $date;
    public $total;
    public $iva;
    public $subTotal;
    public $base;
    public $listProducts = array();

    /**
     * Invoice constructor.
     * @param $reference
     * @param $date
     * @param $total
     * @param $iva
     * @param $subTotal
     * @param $base
     * @param array $listProducts
     */
    public function __construct($subTotal, $iva, $total, array $listProducts)
    {
        $this->reference = $this->createReference();
        $this->date = $this->requestDate();
        $this->total = $total;
        $this->iva = $iva;
        $this->subTotal = $subTotal;
        $this->listProducts = $listProducts;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * @param mixed $iva
     */
    public function setIva($iva)
    {
        $this->iva = $iva;
    }

    /**
     * @return mixed
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param mixed $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @return array
     */
    public function getListProducts(): array
    {
        return $this->listProducts;
    }

    /**
     * @param array $listProducts
     */
    public function setListProducts(array $listProducts)
    {
        $this->listProducts = $listProducts;
    }

    public static function make($subTotal, $iva, $total, array $listProducts){
        return new Invoice($total, $iva, $subTotal, $listProducts);
    }

    public function print($id)
    {
        return $this->__toString();
    }

    private function createReference()
    {
        return rand(1, 20000);
    }

    private function requestDate()
    {
        return time('yyyy-mm-dd hh:mm');
    }

}