<?php

namespace App;

class Store
{

    public $name;
    public $address;
    public $phone;
    public $nit;

    /**
     * Store constructor.
     * @param $name
     * @param $address
     * @param $phone
     * @param $nit
     */
    public function __construct($name, $address, $phone, $nit)
    {
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
        $this->nit = $nit;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * @param mixed $nit
     */
    public function setNit($nit)
    {
        $this->nit = $nit;
    }


}